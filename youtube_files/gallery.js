jQuery.fn.center = function() {
	$(this).load(function() {
		var myWidth = $(this).width();
		$(this).css("width", myWidth + "px");
	});
	return this;
};

/* set of functions to build and run an HTML theater */
var ALTER = {
    'cgif':'http://cfi.thismoment.com/images/clear.gif',    
    'supports_video':function () {
        return !!document.createElement('video').canPlayType;
    },

    'supports_h264_baseline_video': function() {
        if (!ALTER.supports_video()) { return false; }
        var v = document.createElement("video");
        return v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
    },

    'fetchGallery':function(){
        $('#noflash').html('<div class="loads"><img src="'+JS_VARS.loadinggif+'"> Loading Gallery</div>');
        var data = {           
            start: 0,
            total: (JS_VARS.fetchtotal) ? JS_VARS.fetchtotal : 196,
            gallery_id: GALLERY.gallery_id
        },
        url = 'api/base/get_gallery_display.json';
        if (start_content!='') {
            data.start_content_id = start_content;
        }        
      /*  $.post(url,data,ALTER.buildTheater,'json');
		run serializer into a variable
		call buildtheatre with serialized output*/
		result = null;
		$.get(url, function(data) {
				result = data;
			});
		passthis = $.parseJSON(result);
		ALTER.buildTheater(passthis);
		console.trace(passthis);
		/*console.trace(passthis);*/
		
    },

    'buildTheater':function(output){
        if (!window.JS_CONSTANTS) {
            $('#noflash').html('<div class="loads">Sorry, there was an error.<br><br>cr:27</div>');
            return;
        }        
        $('#noflash').hide();
        $('#theater').css('height','auto');        
        var embwrap = $('#embwrap'),
            arrleft = $('<a class="arrs kst left" href="#">left</a>').appendTo(embwrap),
            arright = $('<a class="arrs kst right disabled" href="#">right</a>').appendTo(embwrap),
            mainbox = $('<div id="mainbox"/>').appendTo(embwrap),            
            thumnav = $('<ul id="thumnav"></ul>').appendTo(embwrap),
            thbwrap = $('<div id="thbwrap"/>').appendTo(embwrap),
            thumbox = $('<ul id="thumbox"/>').appendTo(thbwrap),
            thbleft = $('<a class="tharrs kst lef disabled" href="#">left</a>').appendTo(embwrap),
            thbight = $('<a class="tharrs kst rit disabled" href="#">right</a>').appendTo(embwrap);
        GALLERY.mainbox = mainbox;    
        ALTER.categories = output.results.categories;    
        ALTER.catnames = {};
        ALTER.current = 0;
        ALTER.tbcurrent = 0;
        ALTER.tbcount = 4;
        ALTER.contents = output.results.content;
        ALTER.currentthumbs = 0;
        ALTER.series = {}
        GALLERY.autoplay = (output.results.autoplay > 0 && JS_VARS.isFBTab!==true) ? 'autoplay' : 'noauto';
        if (start_content!='')
        $.each(ALTER.contents, function(){
            if (this.content_id == start_content) {
                ALTER.start_cat = this.category_id;
            }
        });        
        var j = 0,
            clon;
        $('body').animate({'opacity':1}, 100, function(){
            var thumnavhtml = '',
                catcharcount = 0;
            $.each(ALTER.categories,function(){
                try{
                    ALTER.catnames[this.category_id] = this.category_name
                    catcharcount+= this.category_name.length;
                    if (ALTER.start_cat && (ALTER.start_cat==this.category_id)) {
                        clon = 'on';
                        ALTER.currentcat = this.category_id;
                    } else if (j==0 && !ALTER.start_cat) {
                        clon = 'on';
                        ALTER.currentcat = this.category_id;
                    } else {
                        clon = '';
                    }            
                    ALTER['ulslide'+this.category_id] = $('<ul class="ulslide cat'+this.category_id+' slide'+clon+'"/>').appendTo(mainbox);
                    thumnavhtml+='<li class="'+clon+'"><a class="emolink '+clon+' n-'+j+'" href="#" rel="'+this.category_id+'">'+this.category_name+'<i></i></a></li>';
                    j++;
                } catch(e){}
            });
            var maxnavchar = (GALLERY.maxnavchars) ? GALLERY.maxnavchars : 180;           
            if (catcharcount > maxnavchar) {
                thumnav.addClass('drop');
                thumnav.html(thumnavhtml).wrap('<div id="dropwrap"/>');
                $('<a href="#" id="currcat">'+thumnav.find('li.on').text()+'<i></i></a>').click(ALTER.showDrop).insertBefore(thumnav);
            } else {
                thumnav.html(thumnavhtml);
            }
                       
            var tbs = 0;
            $.each(ALTER.contents,function(i,obj){
                ALTER.series[i] = this;
                try{
                ALTER['ulslide'+this.category_id].width(ALTER['ulslide'+this.category_id].width()+mainbox.width());  
                }catch(e){}
                var vote = (GALLERY.voteURL && this.user_info) ? '<a class="vote" href="#"><i/>'+GALLERY.voteText+'</a>' : '',
                    userclass = (this.user_info) ? ' ugc ' : '',
                    wrap = $('<li class="wrap ctt_'+this.content_id+' ind_'+i+userclass+'"/>').appendTo(ALTER['ulslide'+this.category_id]),
                    tp = '';
                try {tp = (this.media[0].media_type=='2') ? ' vdo' : '';}catch(e){}
                if ((wrap.index()==0 && ALTER['ulslide'+this.category_id].index()==0)) {                                       
                    ALTER.current = i;
                    wrap.addClass('active');
                    var userinfo = (this.user_info) ? ALTER.userInfo(this.user_info) : '',
                        desc = (GALLERY.htmlincontent) ? decodeURIComponent(decodeURIComponent(this.description)) : this.description;
                    $('<div class="takebox">'+userinfo+'<span class="cat">'+ALTER.catnames[this.category_id]+'</span><h2 class="cu">'+this.title+'</h2><p>'+desc+'</p>'+vote+ALTER.shareTools(this)+'</div>').appendTo(wrap);
                    if (this.media[0].media_type=='2' || this.media[0].media_type=='3') {                       
                        $('<div class="imgbox"><img src="'+this.media[0].large+'"/></div>').hide().appendTo(wrap);
                        if (this.user_info && GALLERY.ugc_vembed_options!=null) {
                            $('<div class="vembed"/>').html(ALTER.addVideo(this.media[0].site_id,this.media[0].site_media_id,GALLERY.ugc_vembed_options)).appendTo(wrap);                            
                        } else {
                            $('<div class="vembed"/>').html(ALTER.addVideo(this.media[0].site_id,this.media[0].site_media_id,GALLERY.vembed_options)).appendTo(wrap);
                        }
                    } else {
                        $('<div class="imgbox"><img src="'+ALTER.cgif+'"/></div>').appendTo(wrap).find('img').attr('src',this.media[0].large).load(ALTER.verticalCenter);
                    }                   
                    ALTER.trackMedia(this.media[0].media_id);                                        
                } else if (i==1 || wrap.index()<=1) {                                
                    ALTER.prebuildContent(this,wrap);
                }
                var ison = ((wrap.index()==0 && ALTER['ulslide'+this.category_id].index()==0)) ? ' on' : '',
                    classon = ison ? ' class="on"' : '',
                    thumb_summary = (this.thumb_summary) ? '<span class="thumb_summary">'+this.thumb_summary+'</span>' : '';
                //if (tbs<ALTER.tbcount*2 || start_content!='') {
                    if (this.category_id==ALTER.currentcat && this.media.length>0) {
                        try{
                            ALTER.buildThumb(classon,ison,tp,this,thumb_summary,thumbox);                           
                            tbs++;
                            ALTER.currentthumbs++;
                            if (ALTER.currentthumbs > ALTER.tbcount) {
                                $('#embwrap .lef').removeClass('disabled');
                            }
                        }catch(e){}    
                    }
                //}
                if (tbs<2) {
                    ALTER.thbwidth = thumbox.find('li').eq(0).width();
                }
                thumbox.width(thumbox.width()+ALTER.thbwidth);
            });

            $('#mainbox h2.cu').removeClass('cu');

            arrleft.click(ALTER.swipeLeft);
            arright.click(ALTER.swipeRight);
            thbleft.click(ALTER.thumbsLeft);
            thbight.click(ALTER.thumbsRight);
            GALLERY.panewidth = mainbox.find('li.wrap').eq(0).width();
            if (start_content!='') {
                // setTimeout(function(){$('#thumbox a[rel="'+start_content+'"]').click();},1400);
            }
            try{updateCufon();}catch(e){}
			
			// Target anything below IE9
			if ($.browser.msie && $.browser.version < 9 ) {
				
				$("#thumnav li").eq(0).css("width", "129px")
									  .css("height", "23px")
									  .css("background-position", "0 -544px").hover(
									function() {
										$(this).css("background-position", "0 -544px");
									},
									function() {
										if( !$(this).hasClass("on") ) { $(this).css("background-position", "0 -570px"); }
									}
				).click(function() {
					$(this).css("background-position", "0 -544px");
					$("#thumnav li").eq(1).css("background-position", "-143px -544px");
				});
									
				$("#thumnav li").eq(1).css("width", "136px")
									  .css("height", "23px")
									  .css("background-position", "-143px -544px").hover(
									function() {
										$(this).css("background-position", "-143px -570px");
									},
									function() {
										if( !$(this).hasClass("on") ) { $(this).css("background-position", "-143px -544px"); }
									}
				).click(function() {
					$(this).css("background-position", "-143px -570px");
					$("#thumnav li").eq(0).css("background-position", "0 -570px")
				});
			}
			
        });        
    },
    
    'verticalCenter': function(image){        
        var img = image.type!='load' ? image : $(this),
            max = img.closest('.imgbox').height(),
            imgh = img.height();            
        if (max>1 && imgh>1 && max!=imgh) {
            img.css('margin-top', (max-imgh)/2);
        }    
    },
    
    'showDrop':function(e){
        e.preventDefault();
        $('#thumnav').show();
    },
    
    'buildThumb':function(classon,ison,tp,item,thumb_summary,thumbox) {
		if (tp !== "") {
			$('<li'+classon+'><a class="ebthumb'+ison+tp+'" href="#" rel="'+item.content_id+'"><img class="imgbox" src="'+item.media[0].thumb+'"/><img class="controls" src="http://cfi.thismoment.com/fabricofourlives/images/controls.png" /><span class="ttl">'+item.title+'</span>'+thumb_summary+'</a></li>').data(item).appendTo(thumbox);
		} else {
			var image_thumb = $('<li'+classon+'><a class="ebthumb'+ison+tp+'" href="#" rel="'+item.content_id+'"><img class="imgbox" src="'+item.media[0].thumb+'" style="height:100px; width: auto; margin: 0 auto;"/><span class="ttl">'+item.title+'</span>'+thumb_summary+'</a></li>').data(item).appendTo(thumbox);
			image_thumb.find(".imgbox").center();
		}
            
    },
        
    'userInfo':function(user_info) {
        return '<div class="userinfo"><a href="'+user_info.user_url+'" target="_blank"><img src="'+user_info.avatar_url+'"/><span>'+user_info.screen_name+'</span></a></div>';
    },
        
    'prebuildContent':function(content, wrap) {             
        try{
            var vote = (GALLERY.voteURL && content.user_info) ? '<a class="vote" href="#"><i/>'+GALLERY.voteText+'</a>' : '',            
                userinfo = (content.user_info) ? ALTER.userInfo(content.user_info) : '',
                desc = (GALLERY.htmlincontent) ? decodeURIComponent(decodeURIComponent(content.description)) : content.description;
            $('<div class="takebox">'+userinfo+'<span class="cat">'+ALTER.catnames[content.category_id]+'</span><h2 class="cu">'+content.title+'</h2><p>'+desc+'</p>'+vote+ALTER.shareTools(content)+'</div>').appendTo(wrap);        
            var imgbox = $('<div class="imgbox"><img src="'+ALTER.cgif+'"/></div>');
            imgbox.appendTo(wrap);
            imgbox.find('img').attr('src',content.media[0].large).load(ALTER.verticalCenter);
        }catch(e){} 
    },
    
    'shareTools': function(content) {
        var html = '<span class="sh">';
        html+= '<a target="_blank" href="'+GALLERY.twurl.replace('$ID', content.content_id).replace('$TAKE', escape(content.title))+'" class="shr kst tw type_Twitter"></a>';
        html+= '<a target="_blank" href="'+GALLERY.fburl.replace('$ID', content.content_id)+'" class="shr kst fb type_Facebook"></a>';
        html+= '<a target="_blank" href="'+GALLERY.myurl.replace('$ID', content.content_id)+'" class="shr kst my type_MySpace"></a>';
        html+= '<a href="'+GALLERY.emurl.replace('$ID', content.content_id).replace('$TAKE', escape(content.title))+'" class="shr kst em type_Email"></a>';
        html+= '</span>'
        return html;
    },
       
    'buildOutThumbs': function(){        
        if (ALTER.thumbsbuilt == 1) {return;}        
        var thumbox = $('#thumbox'),
            thumbcount = thumbox.find('li').length,
            x = 0;
        $.each(ALTER.contents, function(i,obj){
            var tp = (this.media[0].media_type=='2') ? ' vdo' : '',
        thumb_summary = (this.thumb_summary) ? '<span class="thumb_summary">'+this.thumb_summary+'</span>' : '';
            if (i>=thumbcount && x<ALTER.tbcount && (this.category_id==ALTER.currentcat)) {
                ALTER.buildThumb('','',tp,this,thumb_summary,thumbox);               
                thumbox.width(thumbox.width()+ALTER.thbwidth);
                x++;                
            }
        });
        if (thumbox.find('li').length >= ALTER.contents.length) {
            ALTER.thumbsbuilt = 1;
        }
    },

    'swapThumbs':function(e){ // switching to a different category
        e.preventDefault();
        if ($('#thumnav').hasClass('drop')) {
            $('#currcat').html($(this).text()+'<i></i>');
            $('#thumnav').hide();
        }
        $('.emolink').removeClass('on').parent('li').removeClass('on');
        $(this).find("a").addClass('on').parent('li').addClass('on');
        var thumbox = $('#thumbox'),
            x = 0,
            eid = $(this).find("a").attr('rel');
            
        if (eid == "1") {
            $("#emmy_modules").fadeOut("fast", function() {
                $("#camilla_modules").fadeIn();
            });
        } else if (eid == "2" ) {
            $("#camilla_modules").fadeOut("fast", function() {
                $("#emmy_modules").fadeIn();
            });
        }
        
        ALTER.currentcat = eid;
        ALTER.currentthumbs = 0;
        $('#mainbox li.active').removeClass('active');  
        thumbox.fadeOut(200, function(){            
            $(this).empty().css({'left':0}).fadeIn(100, function(){                
                $.each(ALTER.contents,function(){
                    var tp = (this.media[0].media_type=='2') ? ' vdo' : '';
                    var thumb_summary = (this.thumb_summary) ? '<span class="thumb_summary">'+this.thumb_summary+'</span>' : '';
                    if (eid=='all' || this.category_id==eid) {
                        if (this.media && this.media.length>0) {
							if ( tp !== '') {
								$('<li><a class="ebthumb'+tp+'" href="#" rel="'+this.content_id+'"><img class="imgbox" src="'+this.media[0].thumb+'"/><img class="controls" src="http://cfi.thismoment.com/fabricofourlives/images/controls.png"><span class="ttl">'+this.title+'</span>'+thumb_summary+'</a></li>').data(this).appendTo(thumbox); 
							} else {
								var image_thumb = $('<li><a class="ebthumb'+tp+'" href="#" rel="'+this.content_id+'"><img class="imgbox" src="'+this.media[0].thumb+'" style="height:100px; width: auto; margin: 0 auto;"/><span class="ttl">'+this.title+'</span>'+thumb_summary+'</a></li>').data(this).appendTo(thumbox); 								
								image_thumb.find(".imgbox").center();	
							}
                            ALTER.currentthumbs++;
                            thumbox.width(thumbox.width()+ALTER.thbwidth);
                        }
                    }
                });
                $('#embwrap .right').addClass('disabled');
                $('#embwrap .left').removeClass('disabled');
                $('#mainbox .ulslide:visible').fadeOut(200, function(){
                    var slide = $('#mainbox .cat'+eid),
                        wrap = slide.find('li.wrap').eq(0);
                    ALTER.current = wrap.getValueFromClass('ind'); 
                    $(this).removeClass('slideon');
                    thumbox.find('a.ebthumb').eq(0).click();
                    slide.css({'left':0}).fadeIn().addClass('slideon').find('li.wrap').eq(0).addClass('active');
                    
                    ALTER.tbcurrent = 0;
                    $('#embwrap .rit').addClass('disabled');

                    if (ALTER.tbcount<ALTER.currentthumbs) {
                        $('#embwrap .lef').removeClass('disabled');
                    } else {
                        $('#embwrap .lef').addClass('disabled');
                    }
                    ALTER.thumbsbuilt = 1;                    
                });                                
            });            
        });                        
    },

    'swapContent':function(e){ //when someone clicks on a thumbnail
        e.preventDefault();
        var thb = $(this),
            item = thb.parent('li').data(),
            mainbox = $('#mainbox'),
            ulslide = mainbox.find('.cat'+ALTER.currentcat),
            target = ulslide.find('.ctt_'+item.content_id),
            targetpos = target.position().left;
        ulslide.find('.active').removeClass('active');    
        ulslide.fadeOut('fast', function(){    
            $('#thumbox li').removeClass('on');
            $('.ebthumb').removeClass('on').each(function(){
                if(thb.attr('rel')==item.content_id) {
                    thb.addClass('on').parent('li').addClass('on');
                }
            });
            $('.vembed').remove();
            $('#mainbox .imgbox').show();
            if (target.find('img').length<1) {
                ALTER.prebuildContent(item,target);
            }
            ulslide.animate({'left': -targetpos},200,function(){
                ALTER.current = target.getValueFromClass('ind');            
                if (item.media[0].media_type=='2' || item.media[0].media_type=='3') {
                    $('#mainbox .imgbox').hide();
                    if (item.user_info && GALLERY.ugc_vembed_options!=null) {
                        $('<div class="vembed"/>').html(ALTER.addVideo(item.media[0].site_id,item.media[0].site_media_id,GALLERY.ugc_vembed_options)).appendTo(target);                            
                    } else {
                        $('<div class="vembed"/>').html(ALTER.addVideo(item.media[0].site_id,item.media[0].site_media_id,GALLERY.vembed_options)).appendTo(target);
                    }
                }
                //if (ALTER.thumbsbuilt != 1) {ALTER.buildOutThumbs();}
                ALTER.trackMedia(item.media[0].media_id);
                target.addClass('active');
                if (target.index()!=0) {
                    $('#embwrap .right').removeClass('disabled');
                } else {
                    $('#embwrap .right').addClass('disabled');
                }
                try{updateCufon();}catch(e){}
                ulslide.fadeIn('fast',function(){
                    ALTER.verticalCenter(target.find('.imgbox img'));
                });               
            });
        });
    },
    
    'trackMedia': function(media_id) {
        $.getJSON('/api/base/set_media_view.json?media_id='+media_id+'&environment_id='+ENVIRONMENT_ID+'&region_id='+REGION_ID);
    },
    
    'trackShare': function() {
        var type = $(this).getValueFromClass('type'),
            content_id = $(this).parents('li.wrap').getValueFromClass('ctt');
        $.getJSON('/api/base/tracklink.json?environment_id='+ENVIRONMENT_ID+'&region_id='+REGION_ID+'&ref_type=1&ref_type_id=1_Gallery_Share_'+type+'&entity_id='+content_id+'&page_id='+JS_VARS.PAGE_ID);
    },

    'swipeLeft': function(e) {
        try{e.preventDefault();}catch(e){}
        if (GALLERY.motion==1) {return;}
        GALLERY.motion=1;
        $('#embwrap .right').removeClass('disabled');
        if ($('#mainbox li.active').index()+1>=$('#mainbox .cat'+ALTER.currentcat+' li').length) {
            $('#embwrap .left').addClass('disabled');
            GALLERY.motion=0;
            return;
        }
        ALTER.shift(1);
    },

    'swipeRight': function(e) {
        if (GALLERY.motion==1) {return;}
        GALLERY.motion=1;
        try{e.preventDefault();}catch(e){}
        $('#embwrap .left').removeClass('disabled');        
        if ($('#mainbox .active').index()<1) {
            $('#embwrap .right').addClass('disabled');
            GALLERY.motion=0;
            return;
        }
        ALTER.shift(-1);
    },

    'shift': function(change) { //sliding right or left, from clicking on arrows         
        ALTER.current = parseInt(ALTER.current) + change;
        var ulslide = $('#mainbox .cat'+ ALTER.currentcat),
            item = ALTER.series[ALTER.current],
            wrap = ulslide.find('.ctt_'+item.content_id),
            nextwrap = ulslide.find('.wrap').eq(wrap.index() + change),
            nextitem = ALTER.series[ALTER.current + change],
            targetpos = ulslide.position().left-(GALLERY.panewidth * change);           
        ulslide.find('.wrap').removeClass('active'); 
        $('#thumbox li, #thumbox .ebthumb').removeClass('on');
        $('#thumbox a[rel="'+item.content_id+'"]').addClass('on').parent('li').addClass('on');
        GALLERY.mainbox.find('.vembed').css({'visibility':'hidden'});
        $('#mainbox .imgbox').show();
        ulslide.animate({'left':targetpos},400,'swing',function(){
            try{
            GALLERY.mainbox.find('.vembed').remove();
            if (wrap.find('img').length<1) {
                ALTER.prebuildContent(item,wrap);
            }
            if (item.media[0].media_type=='2' || item.media[0].media_type=='3') {
                GALLERY.mainbox.find('.imgbox').hide();
                if (item.user_info && GALLERY.ugc_vembed_options!=null) {
                    $('<div class="vembed"/>').html(ALTER.addVideo(item.media[0].site_id,item.media[0].site_media_id,GALLERY.ugc_vembed_options)).appendTo(wrap);                            
                } else {
                    $('<div class="vembed"/>').html(ALTER.addVideo(item.media[0].site_id,item.media[0].site_media_id,GALLERY.vembed_options)).appendTo(wrap);
                }            }
            wrap.addClass('active');
            if (nextwrap.length && nextitem && nextwrap.find('img').length<1) {
                ALTER.prebuildContent(nextitem,nextwrap);
            }
            try{updateCufon();}catch(e){}
            $('#mainbox h2.cu').removeClass('cu');
            //if (ALTER.thumbsbuilt != 1) {ALTER.buildOutThumbs();}
            ALTER.trackMedia(item.media[0].media_id);
            }catch(e){}
            GALLERY.motion=0;
            
        });
    },
    
    'thumbsLeft':function(e) {        
        try{e.preventDefault();}catch(e){}
        if (ALTER.thumbmotion) {return;}
        if (ALTER.currentthumbs > ALTER.tbcount) {
            $('#embwrap .rit').removeClass('disabled');
        }
        if (ALTER.tbcurrent+ALTER.tbcount >= ALTER.currentthumbs) {
            $('#embwrap .lef').addClass('disabled');
            return;
        }
        ALTER.thumbShift(ALTER.tbcount);        
    },

    'thumbsRight':function(e) {
        try{e.preventDefault();}catch(e){}
        if (ALTER.thumbmotion) {return;}
        if (ALTER.currentthumbs > ALTER.tbcount) {
            $('#embwrap .lef').removeClass('disabled');
        }
        if (ALTER.tbcurrent<1) {
            $('#embwrap .rit').addClass('disabled');
            return;
        }
        ALTER.thumbShift(-ALTER.tbcount);
    },

    'thumbShift': function(change) { // paginating thumbnail carousel
        ALTER.thumbmotion = 1;       
        var ulslide = $('#thumbox');        
        ulslide.animate({'left':ulslide.position().left-(ALTER.thbwidth * change)},400,'swing',function(){            
            if (ALTER.thumbsbuilt != 1) {ALTER.buildOutThumbs();}            
            ALTER.tbcurrent = ALTER.tbcurrent + change;            
            ALTER.thumbmotion = 0;
        });
    },
    
    'addVideo': function(origin, site_media_id, options) {
        var html = '<span>not a video site_id - '+origin+'</span>',
            width,
            height,
            autoplay,
            url,
            settings = {};
        $.extend(settings, options);       
        switch(origin) {
            case '1':
                width = settings.width || 640;
                height = settings.height || 375;
                autoplay = GALLERY.autoplay || 'autoplay';
                html = '<iframe class="youtube-player" id="user-video" type="text/html" width="'+width+'" height="'+height+'" src="http://www.youtube.com/embed/' + site_media_id + '?'+autoplay+'=1&rel=0&wmode=transparent" frameborder="0"></iframe>';
                break;
            case '0':
                width = settings.width || 640;
                height = settings.height || 375;
                autoplay = GALLERY.autoplay || 'autoplay';
                //url = S3_HOSTNAME + '/video/' + site_media_id + '_proc.m4v';
                url = 'http://s3.thismoment.com.s3.amazonaws.com/video/'+GALLERY.s3_video_bucket+'/' + site_media_id + '.mp4';
                if (ALTER.supports_h264_baseline_video()) {
                    html = '<video width="'+width+'" height="'+height+'" src="'+url+'" controls '+autoplay+' />';
                } else {                    
                    var flashVars = 'fpFileURL=' + url + '&embedWidth=' + width + '&embedHeight=' + height + '&videoWidth=' + width + '&videoHeight=' + height;
                    html = '<object width="' + width + '" height="' + height + '">';
                    html+= '<param value="'+url+'" name="movie">';
                    html+= '<param value="transparent" name="wmode">';
                    html+= '<param name="FlashVars" value="' + flashVars + '">';
                    html+= '<embed salign="TL" allowFullScreen = "true" width="' + width + '" height="' + height + '" wmode="transparent" src="'+url+'" flashvars="' + flashVars + '">';
                    html+= '</object>';                    
                }
                break;
        }
        
        return html;
    
    },
    
    'bitlyTweet': function(e) {
        e.preventDefault();
        var lk = $(this),
            tg = lk.attr('href').split('$BTL'),
            child1 = window.open('/redir.html');
            url = '/api/base/shorten_url.json';
        $.getJSON(url + '?url=' + encodeURIComponent(tg[1]), function(output){
            child1.location.href = tg[0]+output.results;
        });    
    },
    
    'castVote': function(e) {
        e.preventDefault();
        var lk = $(this),
            cid = lk.closest('li.wrap').getValueFromClass('ctt');
        $.get(GALLERY.voteURL + cid, function(){
            $('<b class="votethanks">'+GALLERY.thanksForVotingText+'</b>').insertBefore(lk.hide());
        });    
    }, 
    
    'checkWidth': function() {
        var w = $(window).width();
        if (w!=ALTER.windowWidth) {
            ALTER.windowWidth = w;
            ALTER.resizeTheater();            
        }
    },
    
    'resizeTheater': function() {
        if (ALTER.windowWidth <= 960) {
            $('body').removeClass('wide').addClass('narr');
            ALTER.tbcount = 5;
            GALLERY.vembed_options = {
                width:640,
                height:365                   
            }
            $('#mainbox .vembed iframe').attr('width',640);
            GALLERY.panewidth = 760;
            $('#mainbox .cat'+ALTER.currentcat).css('left',(ALTER.current*GALLERY.panewidth) * -1);
        } else {
            $('body').removeClass('narr').addClass('wide');
            ALTER.tbcount = 7;
            GALLERY.vembed_options = {
                width:740,
                height:365                   
            }
            $('#mainbox .vembed iframe').attr('width',720);
            GALLERY.panewidth = 960;
            $('#mainbox .cat'+ALTER.currentcat).css('left',(ALTER.current*GALLERY.panewidth) * -1);
        }     
    } 
}


$(document).ready(function(){   
    if (JS_VARS.device=='tablet') {
        ALTER.checkWidth();
    }
    if ($('#noflash').length) {        
        if (ALTER.supports_h264_baseline_video() || 1==1) {
            ALTER.fetchGallery();
            $('#theater').delegate('.ebthumb','click', ALTER.swapContent);
            $('#theater').delegate('#thumnav li','click', ALTER.swapThumbs);
            $('#theater').delegate('#mainbox a.shr','click', ALTER.trackShare);
            $('#theater').delegate('#mainbox a.type_Twitter','click', ALTER.bitlyTweet);
            $('#theater').delegate('#mainbox .vote','click', ALTER.castVote);
        }        
    }    
    if (JS_VARS.device=='tablet') {
        ALTER.detectWidth = setInterval(ALTER.checkWidth,1000);
    }
    
});