/* constants */
var JS_CONSTANTS={THISMOMENT:0,YOUTUBE:1,FLICKR:2,FACEBOOK:3,MYSPACE:4,YAHOO:5,VIMEO:6,PHOTOBUCKET:7,GOOGLE:8,SMUGMUG:9,SHUTTERFLY:10,TWITTER:11,PLAXO:12,AMAZON:13,FRIENDFEED:14,S3:15,SESAMEVAULT:16,GOOGLE_IMAGES:17,S3_VIDEO:18,WIKIPEDIA:19,FB_CONNECT:20,GOOGLE_ANALYTICS:21,MTV:22,CNNSI:23,INSIDER:24,NYT:25,LIFE:26,GAMESPOT:27,JOOST:28,IMDB:29,ROADANDTRACK:30,YELP:31,CLICK_TYPE_SHARE:1,CLICK_TYPE_LINK:2,CLICK_TYPE_THEATER:3};JS_CONSTANTS.SITENAME={};JS_CONSTANTS.SITENAME[JS_CONSTANTS.THISMOMENT]="thisMoment";JS_CONSTANTS.SITENAME[JS_CONSTANTS.YOUTUBE]="YouTube";JS_CONSTANTS.SITENAME[JS_CONSTANTS.FLICKR]="flickr";JS_CONSTANTS.SITENAME[JS_CONSTANTS.FACEBOOK]="Facebook";JS_CONSTANTS.SITENAME[JS_CONSTANTS.MYSPACE]="MySpace";JS_CONSTANTS.SITENAME[JS_CONSTANTS.YAHOO]="Yahoo!";JS_CONSTANTS.SITENAME[JS_CONSTANTS.VIMEO]="Vimeo";JS_CONSTANTS.SITENAME[JS_CONSTANTS.PHOTOBUCKET]="Photobucket";JS_CONSTANTS.SITENAME[JS_CONSTANTS.GOOGLE]="Picasa";JS_CONSTANTS.SITENAME[JS_CONSTANTS.SMUGMUG]="SmugMug";JS_CONSTANTS.SITENAME[JS_CONSTANTS.SHUTTERFLY]="ShutterFly";JS_CONSTANTS.SITENAME[JS_CONSTANTS.TWITTER]="Twitter";JS_CONSTANTS.SITENAME[JS_CONSTANTS.PLAXO]="Plaxo";JS_CONSTANTS.SITENAME[JS_CONSTANTS.AMAZON]="Amazon";JS_CONSTANTS.SITENAME[JS_CONSTANTS.FRIENDFEED]="FriendFeed";JS_CONSTANTS.SITENAME[JS_CONSTANTS.S3]="thisMoment Photo";JS_CONSTANTS.SITENAME[JS_CONSTANTS.SESAMEVAULT]="thisMoment Video";JS_CONSTANTS.SITENAME[JS_CONSTANTS.GOOGLE_IMAGES]="Google Image Search";JS_CONSTANTS.SITENAME[JS_CONSTANTS.S3_VIDEO]="thisMoment Videos";JS_CONSTANTS.SITENAME[JS_CONSTANTS.MTV]="MTV";JS_CONSTANTS.SITENAME[JS_CONSTANTS.CNNSI]="Sports Illustrated";JS_CONSTANTS.SITENAME[JS_CONSTANTS.INSIDER]="The Insider";JS_CONSTANTS.SITENAME[JS_CONSTANTS.NYT]="New York Times";JS_CONSTANTS.SITENAME[JS_CONSTANTS.LIFE]="Life";JS_CONSTANTS.SITENAME[JS_CONSTANTS.GAMESPOT]="GameSpot";JS_CONSTANTS.SITENAME[JS_CONSTANTS.IMDB]="IMDB";JS_CONSTANTS.SITENAME[JS_CONSTANTS.JOOST]="Joost";JS_CONSTANTS.SITENAME[JS_CONSTANTS.ROADANDTRACK]="Road & Track";JS_CONSTANTS.SITENAME[JS_CONSTANTS.WIKIPEDIA]="Wikipedia";JS_CONSTANTS.SITENAME[JS_CONSTANTS.YELP]="Yelp";JS_CONSTANTS.VIDEO_SITES=[JS_CONSTANTS.S3_VIDEO,JS_CONSTANTS.YOUTUBE,JS_CONSTANTS.JOOST];

/*! Copyright (c) 2010 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers. Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 * Version: 3.0.4   Requires: 1.2.2+
 */
(function(c){var a=["DOMMouseScroll","mousewheel"];c.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var d=a.length;d;){this.addEventListener(a[--d],b,false)}}else{this.onmousewheel=b}},teardown:function(){if(this.removeEventListener){for(var d=a.length;d;){this.removeEventListener(a[--d],b,false)}}else{this.onmousewheel=null}}};c.fn.extend({mousewheel:function(d){return d?this.bind("mousewheel",d):this.trigger("mousewheel")},unmousewheel:function(d){return this.unbind("mousewheel",d)}});function b(i){var g=i||window.event,f=[].slice.call(arguments,1),j=0,h=true,e=0,d=0;i=c.event.fix(g);i.type="mousewheel";if(i.wheelDelta){j=i.wheelDelta/120}if(i.detail){j=-i.detail/3}d=j;if(g.axis!==undefined&&g.axis===g.HORIZONTAL_AXIS){d=0;e=-1*j}if(g.wheelDeltaY!==undefined){d=g.wheelDeltaY/120}if(g.wheelDeltaX!==undefined){e=-1*g.wheelDeltaX/120}f.unshift(i,j,e,d);return c.event.handle.apply(this,f)}})(jQuery);

/* jScrollPane - v2.0.0beta6 - 2010-10-28
 * http://jscrollpane.kelvinluck.com/   
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT and GPL licenses.
 */
(function(b,a,c){b.fn.jScrollPane=function(f){function d(C,L){var au,N=this,V,ah,v,aj,Q,W,y,q,av,aB,ap,i,H,h,j,X,R,al,U,t,A,am,ac,ak,F,l,ao,at,x,aq,aE,g,aA,ag=true,M=true,aD=false,k=false,Z=b.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";aE=C.css("paddingTop")+" "+C.css("paddingRight")+" "+C.css("paddingBottom")+" "+C.css("paddingLeft");g=(parseInt(C.css("paddingLeft"))||0)+(parseInt(C.css("paddingRight"))||0);an(L);function an(aH){var aL,aK,aJ,aG,aF,aI;au=aH;if(V==c){C.css({overflow:"hidden",padding:0});ah=C.innerWidth()+g;v=C.innerHeight();C.width(ah);V=b('<div class="jspPane" />').wrap(b('<div class="jspContainer" />').css({width:ah+"px",height:v+"px"}));C.wrapInner(V.parent());aj=C.find(">.jspContainer");V=aj.find(">.jspPane");V.css("padding",aE)}else{C.css("width","");aI=C.outerWidth()+g!=ah||C.outerHeight()!=v;if(aI){ah=C.innerWidth()+g;v=C.innerHeight();aj.css({width:ah+"px",height:v+"px"})}aA=V.innerWidth();if(!aI&&V.outerWidth()==Q&&V.outerHeight()==W){if(aB||av){V.css("width",aA+"px");C.css("width",(aA+g)+"px")}return}V.css("width","");C.css("width",(ah)+"px");aj.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}aL=V.clone().css("position","absolute");aK=b('<div style="width:1px; position: relative;" />').append(aL);b("body").append(aK);Q=Math.max(V.outerWidth(),aL.outerWidth());aK.remove();W=V.outerHeight();y=Q/ah;q=W/v;av=q>1;aB=y>1;if(!(aB||av)){C.removeClass("jspScrollable");V.css({top:0,width:aj.width()-g});n();D();O();w();af()}else{C.addClass("jspScrollable");aJ=au.maintainPosition&&(H||X);if(aJ){aG=ay();aF=aw()}aC();z();E();if(aJ){K(aG);J(aF)}I();ad();if(au.enableKeyboardNavigation){P()}if(au.clickOnTrack){p()}B();if(au.hijackInternalLinks){m()}}if(au.autoReinitialise&&!aq){aq=setInterval(function(){an(au)},au.autoReinitialiseDelay)}else{if(!au.autoReinitialise&&aq){clearInterval(aq)}}C.trigger("jsp-initialised",[aB||av])}function aC(){if(av){aj.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),b('<div class="jspCap jspCapBottom" />')));R=aj.find(">.jspVerticalBar");al=R.find(">.jspTrack");ap=al.find(">.jspDrag");if(au.showArrows){am=b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",az(0,-1)).bind("click.jsp",ax);ac=b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",az(0,1)).bind("click.jsp",ax);if(au.arrowScrollOnHover){am.bind("mouseover.jsp",az(0,-1,am));ac.bind("mouseover.jsp",az(0,1,ac))}ai(al,au.verticalArrowPositions,am,ac)}t=v;aj.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){t-=b(this).outerHeight()});ap.hover(function(){ap.addClass("jspHover")},function(){ap.removeClass("jspHover")}).bind("mousedown.jsp",function(aF){b("html").bind("dragstart.jsp selectstart.jsp",function(){return false});ap.addClass("jspActive");var s=aF.pageY-ap.position().top;b("html").bind("mousemove.jsp",function(aG){S(aG.pageY-s,false)}).bind("mouseup.jsp mouseleave.jsp",ar);return false});o()}}function o(){al.height(t+"px");H=0;U=au.verticalGutter+al.outerWidth();V.width(ah-U-g);if(R.position().left==0){V.css("margin-left",U+"px")}}function z(){if(aB){aj.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />')));ak=aj.find(">.jspHorizontalBar");F=ak.find(">.jspTrack");h=F.find(">.jspDrag");if(au.showArrows){at=b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",az(-1,0)).bind("click.jsp",ax);x=b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",az(1,0)).bind("click.jsp",ax);if(au.arrowScrollOnHover){at.bind("mouseover.jsp",az(-1,0,at));
x.bind("mouseover.jsp",az(1,0,x))}ai(F,au.horizontalArrowPositions,at,x)}h.hover(function(){h.addClass("jspHover")},function(){h.removeClass("jspHover")}).bind("mousedown.jsp",function(aF){b("html").bind("dragstart.jsp selectstart.jsp",function(){return false});h.addClass("jspActive");var s=aF.pageX-h.position().left;b("html").bind("mousemove.jsp",function(aG){T(aG.pageX-s,false)}).bind("mouseup.jsp mouseleave.jsp",ar);return false});l=aj.innerWidth();ae()}else{}}function ae(){aj.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){l-=b(this).outerWidth()});F.width(l+"px");X=0}function E(){if(aB&&av){var aF=F.outerHeight(),s=al.outerWidth();t-=aF;b(ak).find(">.jspCap:visible,>.jspArrow").each(function(){l+=b(this).outerWidth()});l-=s;v-=s;ah-=aF;F.parent().append(b('<div class="jspCorner" />').css("width",aF+"px"));o();ae()}if(aB){V.width((aj.outerWidth()-g)+"px")}W=V.outerHeight();q=W/v;if(aB){ao=1/y*l;if(ao>au.horizontalDragMaxWidth){ao=au.horizontalDragMaxWidth}else{if(ao<au.horizontalDragMinWidth){ao=au.horizontalDragMinWidth}}h.width(ao+"px");j=l-ao;ab(X)}if(av){A=1/q*t;if(A>au.verticalDragMaxHeight){A=au.verticalDragMaxHeight}else{if(A<au.verticalDragMinHeight){A=au.verticalDragMinHeight}}ap.height(A+"px");i=t-A;aa(H)}}function ai(aG,aI,aF,s){var aK="before",aH="after",aJ;if(aI=="os"){aI=/Mac/.test(navigator.platform)?"after":"split"}if(aI==aK){aH=aI}else{if(aI==aH){aK=aI;aJ=aF;aF=s;s=aJ}}aG[aK](aF)[aH](s)}function az(aF,s,aG){return function(){G(aF,s,this,aG);this.blur();return false}}function G(aH,aF,aK,aJ){aK=b(aK).addClass("jspActive");var aI,s=function(){if(aH!=0){T(X+aH*au.arrowButtonSpeed,false)}if(aF!=0){S(H+aF*au.arrowButtonSpeed,false)}},aG=setInterval(s,au.arrowRepeatFreq);s();aI=aJ==c?"mouseup.jsp":"mouseout.jsp";aJ=aJ||b("html");aJ.bind(aI,function(){aK.removeClass("jspActive");clearInterval(aG);aJ.unbind(aI)})}function p(){w();if(av){al.bind("mousedown.jsp",function(aH){if(aH.originalTarget==c||aH.originalTarget==aH.currentTarget){var aG=b(this),s=setInterval(function(){var aI=aG.offset(),aJ=aH.pageY-aI.top;if(H+A<aJ){S(H+au.trackClickSpeed)}else{if(aJ<H){S(H-au.trackClickSpeed)}else{aF()}}},au.trackClickRepeatFreq),aF=function(){s&&clearInterval(s);s=null;b(document).unbind("mouseup.jsp",aF)};b(document).bind("mouseup.jsp",aF);return false}})}if(aB){F.bind("mousedown.jsp",function(aH){if(aH.originalTarget==c||aH.originalTarget==aH.currentTarget){var aG=b(this),s=setInterval(function(){var aI=aG.offset(),aJ=aH.pageX-aI.left;if(X+ao<aJ){T(X+au.trackClickSpeed)}else{if(aJ<X){T(X-au.trackClickSpeed)}else{aF()}}},au.trackClickRepeatFreq),aF=function(){s&&clearInterval(s);s=null;b(document).unbind("mouseup.jsp",aF)};b(document).bind("mouseup.jsp",aF);return false}})}}function w(){F&&F.unbind("mousedown.jsp");al&&al.unbind("mousedown.jsp")}function ar(){b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");ap&&ap.removeClass("jspActive");h&&h.removeClass("jspActive")}function S(s,aF){if(!av){return}if(s<0){s=0}else{if(s>i){s=i}}if(aF==c){aF=au.animateScroll}if(aF){N.animate(ap,"top",s,aa)}else{ap.css("top",s);aa(s)}}function aa(aF){if(aF==c){aF=ap.position().top}aj.scrollTop(0);H=aF;var aI=H==0,aG=H==i,aH=aF/i,s=-aH*(W-v);if(ag!=aI||aD!=aG){ag=aI;aD=aG;C.trigger("jsp-arrow-change",[ag,aD,M,k])}u(aI,aG);V.css("top",s);C.trigger("jsp-scroll-y",[-s,aI,aG])}function T(aF,s){if(!aB){return}if(aF<0){aF=0}else{if(aF>j){aF=j}}if(s==c){s=au.animateScroll}if(s){N.animate(h,"left",aF,ab)}else{h.css("left",aF);ab(aF)}}function ab(aF){if(aF==c){aF=h.position().left}aj.scrollTop(0);X=aF;var aI=X==0,aH=X==j,aG=aF/j,s=-aG*(Q-ah);if(M!=aI||k!=aH){M=aI;k=aH;C.trigger("jsp-arrow-change",[ag,aD,M,k])}r(aI,aH);V.css("left",s);C.trigger("jsp-scroll-x",[-s,aI,aH])}function u(aF,s){if(au.showArrows){am[aF?"addClass":"removeClass"]("jspDisabled");ac[s?"addClass":"removeClass"]("jspDisabled")}}function r(aF,s){if(au.showArrows){at[aF?"addClass":"removeClass"]("jspDisabled");
x[s?"addClass":"removeClass"]("jspDisabled")}}function J(s,aF){var aG=s/(W-v);S(aG*i,aF)}function K(aF,s){var aG=aF/(Q-ah);T(aG*j,s)}function Y(aR,aM,aG){var aK,aH,aI,s=0,aQ=0,aF,aL,aO,aN,aP;try{aK=b(aR)}catch(aJ){return}aH=aK.outerHeight();aI=aK.outerWidth();aj.scrollTop(0);aj.scrollLeft(0);while(!aK.is(".jspPane")){s+=aK.position().top;aQ+=aK.position().left;aK=aK.offsetParent();if(/^body|html$/i.test(aK[0].nodeName)){return}}aF=aw();aL=aF+v;if(s<aF||aM){aN=s-au.verticalGutter}else{if(s+aH>aL){aN=s-v+aH+au.verticalGutter}}if(aN){J(aN,aG)}viewportLeft=ay();aO=viewportLeft+ah;if(aQ<viewportLeft||aM){aP=aQ-au.horizontalGutter}else{if(aQ+aI>aO){aP=aQ-ah+aI+au.horizontalGutter}}if(aP){K(aP,aG)}}function ay(){return -V.position().left}function aw(){return -V.position().top}function ad(){aj.unbind(Z).bind(Z,function(aI,aJ,aH,aF){var aG=X,s=H;T(X+aH*au.mouseWheelSpeed,false);S(H-aF*au.mouseWheelSpeed,false);return aG==X&&s==H})}function n(){aj.unbind(Z)}function ax(){return false}function I(){V.unbind("focusin.jsp").bind("focusin.jsp",function(s){if(s.target===V[0]){return}Y(s.target,false)})}function D(){V.unbind("focusin.jsp")}function P(){var aF,s;C.attr("tabindex",0).unbind("keydown.jsp").bind("keydown.jsp",function(aJ){if(aJ.target!==C[0]){return}var aH=X,aG=H,aI=aF?2:16;switch(aJ.keyCode){case 40:S(H+aI,false);break;case 38:S(H-aI,false);break;case 34:case 32:J(aw()+Math.max(32,v)-16);break;case 33:J(aw()-v+16);break;case 35:J(W-v);break;case 36:J(0);break;case 39:T(X+aI,false);break;case 37:T(X-aI,false);break}if(!(aH==X&&aG==H)){aF=true;clearTimeout(s);s=setTimeout(function(){aF=false},260);return false}});if(au.hideFocus){C.css("outline","none");if("hideFocus" in aj[0]){C.attr("hideFocus",true)}}else{C.css("outline","");if("hideFocus" in aj[0]){C.attr("hideFocus",false)}}}function O(){C.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp")}function B(){if(location.hash&&location.hash.length>1){var aG,aF;try{aG=b(location.hash)}catch(s){return}if(aG.length&&V.find(aG)){if(aj.scrollTop()==0){aF=setInterval(function(){if(aj.scrollTop()>0){Y(location.hash,true);b(document).scrollTop(aj.position().top);clearInterval(aF)}},50)}else{Y(location.hash,true);b(document).scrollTop(aj.position().top)}}}}function af(){b("a.jspHijack").unbind("click.jsp-hijack").removeClass("jspHijack")}function m(){af();b("a[href^=#]").addClass("jspHijack").bind("click.jsp-hijack",function(){var s=this.href.split("#"),aF;if(s.length>1){aF=s[1];if(aF.length>0&&V.find("#"+aF).length>0){Y("#"+aF,true);return false}}})}b.extend(N,{reinitialise:function(aF){aF=b.extend({},aF,au);an(aF)},scrollToElement:function(aG,aF,s){Y(aG,aF,s)},scrollTo:function(aG,s,aF){K(aG,aF);J(s,aF)},scrollToX:function(aF,s){K(aF,s)},scrollToY:function(s,aF){J(s,aF)},scrollBy:function(aF,s,aG){N.scrollByX(aF,aG);N.scrollByY(s,aG)},scrollByX:function(s,aG){var aF=ay()+s,aH=aF/(Q-ah);T(aH*j,aG)},scrollByY:function(s,aG){var aF=aw()+s,aH=aF/(W-v);S(aH*i,aG)},animate:function(aF,aI,s,aH){var aG={};aG[aI]=s;aF.animate(aG,{duration:au.animateDuration,ease:au.animateEase,queue:false,step:aH})},getContentPositionX:function(){return ay()},getContentPositionY:function(){return aw()},getIsScrollableH:function(){return aB},getIsScrollableV:function(){return av},getContentPane:function(){return V},scrollToBottom:function(s){S(i,s)},hijackInternalLinks:function(){m()}})}f=b.extend({},b.fn.jScrollPane.defaults,f);var e;this.each(function(){var g=b(this),h=g.data("jsp");if(h){h.reinitialise(f)}else{h=new d(g,f);g.data("jsp",h)}e=e?e.add(g):g});return e};b.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,animateScroll:false,animateDuration:300,animateEase:"linear",hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:10,arrowButtonSpeed:10,arrowRepeatFreq:100,arrowScrollOnHover:false,trackClickSpeed:30,trackClickRepeatFreq:100,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:true,hideFocus:false}
})(jQuery,this);

var isiPad = navigator.userAgent.match(/iPad/i) != null;
var isTablet = (navigator.userAgent.match(/iPad/i) != null || navigator.userAgent.match(/android/i) != null);

/*
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie=function(b,j,m){if(typeof j!="undefined"){m=m||{};if(j===null){j="";m=$.extend({},m);m.expires=-1}var e="";if(m.expires&&(typeof m.expires=="number"||m.expires.toUTCString)){var f;if(typeof m.expires=="number"){f=new Date();f.setTime(f.getTime()+(m.expires*24*60*60*1000))}else{f=m.expires}e="; expires="+f.toUTCString()}var l=m.path?"; path="+(m.path):"";var g=m.domain?"; domain="+(m.domain):"";var a=m.secure?"; secure":"";document.cookie=[b,"=",encodeURIComponent(j),e,l,g,a].join("")}else{var d=null;if(document.cookie&&document.cookie!=""){var k=document.cookie.split(";");for(var h=0;h<k.length;h++){var c=jQuery.trim(k[h]);if(c.substring(0,b.length+1)==(b+"=")){d=decodeURIComponent(c.substring(b.length+1));break}}}return d}};


(function($){function toIntegersAtLease(n)
{return n<10?'0'+n:n;}
Date.prototype.toJSON=function(date)
{return date.getUTCFullYear()+'-'+
toIntegersAtLease(date.getUTCMonth()+1)+'-'+
toIntegersAtLease(date.getUTCDate());};var escapeable=/["\\\x00-\x1f\x7f-\x9f]/g;var meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'}
$.quoteString=function(string)
{if(escapeable.test(string))
{return'"'+string.replace(escapeable,function(a)
{var c=meta[a];if(typeof c==='string'){return c;}
c=a.charCodeAt();return'\\u00'+Math.floor(c/16).toString(16)+(c%16).toString(16);})+'"'}
return'"'+string+'"';}
$.toJSON=function(o)
{var type=typeof(o);if(type=="undefined")
return"undefined";else if(type=="number"||type=="boolean")
return o+"";else if(o===null)
return"null";if(type=="string")
{return $.quoteString(o);}
if(type=="object"&&typeof o.toJSON=="function")
return o.toJSON();if(type!="function"&&typeof(o.length)=="number")
{var ret=[];for(var i=0;i<o.length;i++){ret.push($.toJSON(o[i]));}
return"["+ret.join(", ")+"]";}
if(type=="function"){throw new TypeError("Unable to convert object of type 'function' to json.");}
ret=[];for(var k in o){var name;var type=typeof(k);if(type=="number")
name='"'+k+'"';else if(type=="string")
name=$.quoteString(k);else
continue;val=$.toJSON(o[k]);if(typeof(val)!="string"){continue;}
ret.push(name+": "+val);}
return"{"+ret.join(", ")+"}";}
$.evalJSON=function(src)
{return eval("("+src+")");}
$.secureEvalJSON=function(src)
{var filtered=src;filtered=filtered.replace(/\\["\\\/bfnrtu]/g,'@');filtered=filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']');filtered=filtered.replace(/(?:^|:|,)(?:\s*\[)+/g,'');if(/^[\],:{}\s]*$/.test(filtered))
return eval("("+src+")");else
throw new SyntaxError("Error parsing JSON, source is not valid.");}})(jQuery);


$.extend({
    // build a video player, this will replace the individual functions
    'buildVideoEmbed': function (site_id, media_id, options) {
        var settings = {};
        $.extend(settings, options);
        var html = '<span>not a video site_id - '+site_id+'</span>';
        var width, height, url;
        switch (site_id) {
        case JS_CONSTANTS.YOUTUBE:
            width = settings.width || 624;
            height = settings.height || 375;
            url = settings.url || 'http://www.youtube.com/v/' + media_id;
            url+= '&autoplay=1&rel=0&iv_load_policy=3';
            html = '<object width="' + width + '" height="' + height + '">';
            html+= '<param name="movie" value="' + url + '">';
            html+= '<param value="transparent" name="wmode">';
            html+= '<param name="allowFullScreen" value="true">';
            html+= '<embed src="' + url + '" type="application/x-shockwave-flash" allowfullscreen="true" wmode="transparent" width="' + width + '" height="' + height + '">';
            html+= '</object>';
            break;
        }
        return $(html);
    }
});

$.fn.extend({
    // get a value from a class
    'getValueFromClass': function(c) {
        if (!$(this).attr('class')) {
            return '';
        }
        var classes = $(this).attr('class').split(' ');
        for (i = 0; i < classes.length; i++) {
            if (classes[i].indexOf(c) != -1) {
                return classes[i].split(c + '_')[1];
            }
        }
        return '';
    }
});

function clearField(e) {
    var i = $(e.target).val();
    if (!$(e.target).hasClass('cleared')) {
        $(e.target).val("").addClass('cleared');
    }
}

var dclick = 0,
    tweetbox,
    MEDIA = {},
    form,
    sign;

// check for !UPDATES if mod-col isn't in there, and UPDATES.successful_posts if using old mod-col
if (!UPDATES) {
    var UPDATES = {
        'successful_posts': []
    };
}
else if (!UPDATES.successful_posts) { UPDATES.successful_posts = [];}

/* comments functions */
var COMMENT = {
    'showform':function(e){
        e.preventDefault();
        //stream.lock = 1;
        var li = $(this).parents('li.s-item');
        if (li.find('#cform').length) {
            COMMENT.cancel();
            return;
        }
        $('#cform').slideUp('fast',function(){
            $(this).css({'position':'static','left':0,'top':0})
                .insertAfter(li.find('.acts').last()).end()
                .find('textarea').val('').end()
                .slideDown('fast');
        });
        $('#cform textarea').unbind().keydown(function(e){            
            if (e.which==13) { COMMENT.save(); }
        });
    },
    'outlink':function(e){
        var site_id = $(this).getValueFromClass('site'),
            site_name = '';
        switch (parseInt(site_id)){
            case 1:
                site_name = 'YouTube';
                break;
            case 4:
                site_name = 'MySpace';
                break;
            case 11:
                site_name = 'Twitter';
                break;
        }
        var checking = $('<div id="checking" />').html('<img src="'+JS_VARS.loadinggif+'"> Connecting to '+site_name+'... &nbsp;').appendTo(sign);
        $('<a href="#">Cancel</a>').click(function(e){
            e.preventDefault();
            clearChecklink();
        }).appendTo(checking);
        MEDIA.site_id = site_id;
        MEDIA.checklink = setTimeout(checkLink,2000);
    },
    'fblink':function(e){
        e.preventDefault();               
        DEC.fbauth(false);        
    },
    'save':function(e){
        try{e.preventDefault();}catch(e){}
        var cform = $('#cform'),         
            li = cform.parents('li.s-item'),
            comment = cform.find('textarea').val(),
            data = {
                'comment':comment,                
                'stream_post_id':li.attr('rel'),
                'stream_id':li.getValueFromClass('id'),
                'stream_module_id':UPDATES.stream_widget_id
            };
        if (li.hasClass('source_11')) {
            data.reply_user_name = li.find('.wrap .title').text();
        }    
        if (data.comment.length<1) {return false;}
        $.post('/api/stream/post_comment.json', data, function(output){            
            var comment = COMMENT.build(data, output.results, true);
            COMMENT.cancel();
            if (li.find('ul.coms').length) {
                comment.prependTo(li.find('ul.coms').eq(0));
            } else {
                $('<ul class="coms"/>').append(comment).appendTo(li.find('.wrap'));
            }
            var jscOptions = jscOptions || {};
            jscOptions['verticalDragMinHeight'] = 92;
            jscOptions['verticalDragMaxHeight'] = 92;
            if (!isTablet) {
                tweetbox.jScrollPane(jscOptions);
            }
        },'json');
    },
    'build':function(data, output, editable) {
        try{
            var comment = $('<li class="com cid_'+output.comment_id+'"></li>');           
            if  (JS_VARS.acct || editable===true || (user && (
                (user.facebook_id && user.facebook_id == output.user.facebook_id) ||
                (user.twitter_id && user.twitter_id == output.user.twitter_id) ||
                (user.google_id && user.google_id == output.user.google_id) ||
                (user.youtube_id && user.youtube_id == output.user.youtube_id) ||
                (user.myspace_id && user.myspace_id == output.user.myspace_id)
            ))) {
                $('<a href="#" class="delete">(X)</a>').appendTo(comment);
            }
            $('<p class="user"><img src="'+output.attribution_avatar+'"><a target=_blank" href="'+output.attribution_url+'">'+output.attribution_author+'</a>:</p>').appendTo(comment);
            $('<p class="text"/>').text(data.comment).appendTo(comment);
            $('<span class="time">'+output.formatted_date+'</span>').appendTo(comment);            
            return comment;
        }catch(e){console.log(e);}
    },
    'cancel':function(e) {
        if (e) {e.preventDefault();}
        //stream.lock = 0;
        $('#cform').hide().appendTo('#twitter_stream_box').end().find('textarea').val('');
    },
    'remove': function(e) {
        e.preventDefault();
        var lk = $(this),
            com = lk.parents('li.com'),           
            url = '/api/stream/delete_comment.json',
            data = {
                'comment_id': com.getValueFromClass('cid'),
                'stream_id':com.parents('li.s-item').getValueFromClass('id'),
                'stream_post_id':com.parents('li.s-item').attr('rel')
            }
        $.post(url,data,function(){
            com.fadeOut('fast',function(){com.remove()});
        },'json');
    },
    'more': function(e) {
        e.preventDefault();
        var stm = $(this).parents('li.s-item'),
            sid = stm.attr('rel'),
            src = stm.getValueFromClass('source'),
            str = stm.getValueFromClass('id'),
            coms = stm.find('ul.coms'),
            mor = stm.find('.morecomments').html('<img src="'+JS_VARS.loadinggif+'"/>');
        $.getJSON('/api/stream/see_all_comments.json?stream_post_id='+sid+'&stream_id='+str+'&site_id='+src, function(output){
            mor.fadeOut();
            if (output.results) {
                coms.empty();               
                $.each(output.results, function(){
                    if (this.attribution_author) {
                        var comment = COMMENT.build(this, this);
                        comment.hide();
                        coms.append(comment);
                        comment.fadeIn('slow');
                    }
                });
                if (!isTablet) {
                    var jscOptions = jscOptions || {};
                    jscOptions['verticalDragMinHeight'] = 92;
                    jscOptions['verticalDragMaxHeight'] = 92;
                    tweetbox.jScrollPane(jscOptions);
                }    
            } else {                
                console.log(output);
            }
        });
    }
}

var DEC = {
    'fbauth': function(post){
        FB.getLoginStatus(function(response) {
            if (response.authResponse) {
                if ((UPDATES && UPDATES.post_to_service && UPDATES.post_to_service!='')) {
                    if (response.perms && response.perms.indexOf('publish_stream')!=-1) {
                        // user is logged in and granted publish_stream permissions.
                        DEC.postAuth(post, response);
                    } else {
                        // user is logged in, but did not yet grant permission
                        FB.login(function(response) {
                            if (response.authResponse) {
                                DEC.postAuth(post, response);
                            } else {
                                // user cancelled login
                            }
                        }, {scope:'publish_stream'});
                    }
                } else {                    
                    DEC.postAuth(post, response);
                }
            } else {
                var permissions = (UPDATES && UPDATES.post_to_service && UPDATES.post_to_service!='') ? 'publish_stream' : '';
                FB.login(function(response) {
                    if (response.authResponse) {
                        DEC.postAuth(post, response);
                    } else {
                    // user cancelled login
                    }
                }, {scope:permissions}); 
            }
        });
    },
    
    'postAuth': function(post, response){
        console.log("postAuth on linke 314 is broken");     
        $.getJSON(SIGNAL_URL + '/login/callback_facebook?from_js=1&session='+$.toJSON(response.authResponse), function(output){
            $('#cform, #streampost').addClass('linked');
            JS_VARS.linked = true;
            DEC.showFBLogout();
            UPDATES.logged_into = 'facebook';
            if (post===true) {
    
                // STREAM AUTH V1
                if (!$('#streampost').hasClass('stream_auth_v2')) {
                    POST.send();
                }
                
                // STREAM AUTH V2
                else {
                    POST.userInfo = output;
                    var spost = $('#streampost textarea');
                    if (POST.link_process == 'msg_post') {
                        POST.send();
                    }
                    else if (POST.link_process == 'empty_post' || POST.link_process == 'empty_signin') {
                        //spost.removeClass('cleared');
                        POST.fillUserInfo(output, UPDATES.streampost_cta);
                    }
                    else if (POST.link_process == 'msg_signin') {
                        POST.fillUserInfo(output, spost.val());
                    }
                }
            }
            else if (post) {
                try{post();}catch(e){}
            }
        });
    },
    
    'showFBLogout': function(){
        $('.fblogout').click(function(e){
            e.preventDefault();
            FB.logout();
            $(this).hide();
        }).show();
        if ($('.standalone').length) {
            try{ parent.DEC.showFBLogout(); }catch(e){}
        }
    }
}

/* posting functions */
var POST = {
    
    'link_process':'',
    'avatar_built': false,
    'userInfo': {},
    
    'save': function(e) {
        try{e.preventDefault();}catch(e){}
        var spost = $('#streampost textarea');

        // STREAM V1
        if (!$('#streampost').hasClass('stream_auth_v2')) {
            console.log("stream_v1 crazytime!");
            if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {return;}
            if (JS_VARS.linked==true || JS_VARS.linked == JS_CONSTANTS.TWITTER) {
                POST.send();
            } else {
                $('#streampost .nolink').show();
                $('#streampost .entry').hide();
            }
        }
        
        // STREAM V2
        else {
            console.log("stream_v2 correct!");
            if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {
                console.log("empty_post");
                POST.link_process = 'empty_post';
            }
            else {
                console.log("msg_post");
                POST.link_process = 'msg_post';
            }
            if (JS_VARS.linked==true || JS_VARS.linked == JS_CONSTANTS.TWITTER) {
                console.log("Oh we're about to send if POST.link_process is msg_post")
                if (POST.link_process == 'msg_post') {
                    POST.send();
                }
                else { return; }
            } else {
                $('#streampost .nolink').show();
                $('#streampost .entry').hide();
            }
        }
    },

    'outlink':function(e){
        var site_id = $(this).getValueFromClass('site'),
            site_name = '';

        // STREAM V2
        if ($('#streampost').hasClass('stream_auth_v2')) {
            if ($(this).attr('rel') == 'step1' || POST.link_process == 'change_user') {
                var spost = $('#streampost textarea');
                if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {
                    POST.link_process = 'empty_signin';
                }
                else {
                    POST.link_process = 'msg_signin';
                }
            }
        }


        switch (parseInt(site_id)){
            case 1:
                site_name = 'YouTube';
                UPDATES.logged_into = 'youtube';
                break;
            case 4:
                site_name = 'MySpace';
                UPDATES.logged_into = 'myspace';
                break;
            case 11:
                site_name = 'Twitter';
                UPDATES.logged_into = 'twitter';
                break;
        }
        MEDIA.site_id = site_id;
        MEDIA.checklink = setTimeout(checkLink,2000);
    },
    'fblink':function(e){
        e.preventDefault();

        // STREAM V2
        if ($('#streampost').hasClass('stream_auth_v2')) {
            var spost = $('#streampost textarea');
            if ($(this).attr('rel') == 'step1') {


                if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {
                    POST.link_process = 'empty_signin';
                }
                else {
                    POST.link_process = 'msg_signin';
                }
            }
            else {
                if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {
                    POST.link_process = 'empty_post';
                }
                else {
                    POST.link_process = 'msg_post';
                }
            }
        }

        DEC.fbauth(true);
    },


    // STREAM V2 FUNCTIONS - rebuild linked streampost area with current textarea contents, avatar img, username, Post To Facebook opt-out (if post-to-service enabled). Also logout + change-user options
    'fillUserInfo': function(user_info, message) {
        
        // cover for facebook returned json's different format
        if (user_info.linked) {
            var user_stats = user_info.linked.info;
        }
        else {
            user_stats = user_info;
        }
        
        $('#streampost .entry').fadeOut(600, function(){
            $('#streampost .nolink').hide();
            $('#streampost .step1_signin_cta').hide();
            
            if (UPDATES.logged_into == 'facebook') {
                $('#streampost .fb_optout').show();
                $('#streampost .fb_optout label').text('Post to Facebook');
            }
            else if (UPDATES.logged_into == 'twitter') {
                $('#streampost .fb_optout').show();
                $('#streampost .fb_optout label').text('Post to Twitter');
            }
            else {
                $('#streampost .fb_optout').hide();
            }
            
            $('#streampost .avatar').attr('src', user_stats.avatar_url).css({'height' : '40px', 'width' : '40px'});
            $('#streampost .posting_as_id').prepend('Posting as ' + user_stats.screen_name + ' ');
            $('#streampost .linked_state_items').show();
            $('#streampost .entry').fadeIn('slow');
            $('#streampost').fadeIn('slow');
        });
        
        POST.avatar_built = true;
    },
    
    'change_user': function(e) {
        e.preventDefault();
        if (UPDATES.logged_into == 'facebook') {
            $('.fblogout').eq(0).click();
        }
        POST.link_process = 'change_user';
        $('#streampost .posting_as_id').text('');
        
        $('#cform, #streampost, .streampost').removeClass('linked');
        
        UPDATES.logged_into = 'none';
        JS_VARS.linked = false;
        POST.avatar_built = false;
        
        
        var spost = $('#streampost textarea');
        if (!spost.hasClass('cleared') || spost.val()=='' || spost.val()==' ' || spost.val()=='  ') {
            POST.link_process = 'empty_signin';
        }
        else {
            POST.link_process = 'msg_signin';
            spost.addClass('cleared');
        }
        
        $('#streampost .nolink').show();
        $('#streampost .entry').hide();
        
    },
    
    'streampost_logout': function(e) {
        e.preventDefault();
        if (UPDATES.logged_into == 'facebook') {
            $('.fblogout').eq(0).click();
        }
        
        $('#streampost').fadeOut(600, function(){
            $('#streampost .step1_signin_cta').show();
            $('#streampost .linked_state_items').hide();
            $('#streampost .posting_as_id').text('');
            $('#streampost .avatar').attr('src', 'http://cfi.thismoment.com/images/dec/new_stream_default_avatar.png');
            $('#streampost').fadeIn('slow');
        });
        
        
        $('#cform, #streampost, .streampost').removeClass('linked');
        
        UPDATES.logged_into = 'none';
        JS_VARS.linked = false;
        POST.avatar_built = false;
    },
    // END STREAM V2 NEW FUNCTIONS


    'send': function() {
        console.log("POST.send() called");
        var textarea = $('#streampost textarea'),
            tab = $('#streamnav li.on a'),
            data = {
                'content_name':textarea.val(),
                'stream_module_id': UPDATES.stream_widget_id,
                'stream_id': (tab.hasClass('postable')) ? tab.getValueFromClass('id') : UPDATES.post_stream_id,
                'share_url': UPDATES.share_url 
            }
    
        if (UPDATES.logged_into == 'facebook' && $('#streampost').hasClass('stream_auth_v2')) {
            data.fb_share_url = UPDATES.fb_share_url;
        }
    
        if ($('#optout').length && $('#optout:checked').length<1) {
            data.opt_out = 1;
        }
        JS_VARS.streampost = data.description;
        if ($('#streampost').hasClass('stream_auth_v2')) {
            textarea.val('');
        }
        else {
            textarea.val('').removeClass('cleared');
        }
        
        console.log("about to call add_to_stream");
        $.post('/api/stream/add_to_stream.json', data, function(output){
            console.log("in add_to_stream post callback");
            if (output.results) {
                console.log("we have output.results!");
                POST.build(output.results);
                UPDATES.successful_posts.push(output.results.stream_post_id);
            }
            $('#streampost').fadeOut('slow', function(){
    
                // STREAM V1
                if (!$('#streampost').hasClass('stream_auth_v2')) {
                    $('#streampost .nolink').hide();
                    $('#streampost .entry').show();
                    $(this).fadeIn('slow');
                }
                // STREAM V2
                else {
                    console.log("LINE 604!")
                    $('#streampost .nolink').hide();
                    if (POST.avatar_built) {
                        $('#streampost .entry').show();
                        $(this).fadeIn('slow');
                    }
                    else {
                        console.log("who likes cookies?");
                        POST.fillUserInfo(POST.userInfo, UPDATES.streampost_cta);
                    }
                }
            });
        }, 'json');
    },
    'remove': function(e){
        e.preventDefault();
        var dlink = $(this),
            li = dlink.parents('li.s-item'),
            data = {
                'stream_post_id' : $(this).attr('rel'),
                'stream_id': li.getValueFromClass('id')
            }
        $.post(dlink.attr('href'),data,function(){
            li.fadeOut();
        },'json');
    },
    'ban': function(e){
        e.preventDefault();
        var dlink = $(this),
            li = dlink.parents('li.s-item');            
        $.getJSON(dlink.attr('href'),function(){
            li.fadeOut();
        });
    },
    'build':function(output, html, bin) {
        if (!output.content_name && !output.description) {console.log(output); return;}
        if (!output.content_name) {output.content_name = output.description;}
        if (!output.stream_source) {output.stream_source = 0;}
        var spost = $('<li class="twitter-reply s-item source_'+output.stream_source+'"/>').hide().attr('rel', output.stream_post_id);        
        $('<img src="'+output.attribution_avatar+'" class="avatar">').appendTo(spost);
        var wrap = $('<div class="wrap"/>');
        $('<a target="_blank" class="title" href="'+output.attribution_url+'"/>').text(output.attribution_author).appendTo(wrap);
        if (html) {
            $('<span/>').html(' ' + output.content_name).appendTo(wrap);
        } else {
            $('<span/>').text(' ' + output.content_name).appendTo(wrap);
        }
        $('<br/>').appendTo(wrap);
        $('<span class="created"/>').html('<a target="_blank" href="'+TRACK.buildTrackLink(output.attribution_url,'stream_click_'+JS_CONSTANTS.SITENAME[output.stream_source].toLowerCase(), 0, 4)+'" class="icon kst icon_'+output.stream_source+'"></a> '+output.formatted_date).appendTo(wrap);
        var acts = $('<span class="acts"/>');
        if (UPDATES.allowcomments && !STREAM.autoupdateloop) {
            $('<a href="#" class="addcomment"/>').text('comment').appendTo(acts);
        }
        acts.appendTo(wrap);
        if (JS_VARS.acct) {
            $('<span class="acts"><a href="/api/stream/delete_stream_content.json" rel="'+output.stream_post_id+'" class="deletestreampost">(X)</a> <a class="banstreampost act" href="/api/stream/ban_user.json?site_id='+output.stream_source+'&site_user_id='+output.user_source_id+'">(ban)</a></span>').appendTo(wrap);    
        }        
        wrap.appendTo(spost);
        $('<div class="cb"/>').appendTo(spost);
        if (bin) {
            spost.prependTo(bin);
        } else {
            spost.prependTo($('.tweetfeed:first'));
        }
        spost.fadeIn('slow');
    }
}

var RETWEET = {
    tweet_id: '',
    tweet_post: '',
    'checkAuth': function(e) {
        e.preventDefault();
        RETWEET.tweet_post = $(this).parents('li.s-item');
        if ($(this).hasClass('done')) {return;}
        RETWEET.tweet_id = RETWEET.tweet_post.attr('rel');
        if (JS_VARS.linked && JS_VARS.linked == JS_CONSTANTS.TWITTER) {
            // linked already, post it
            RETWEET.postRetweet();
        } else {
            // not linked twitter, pop auth
            MEDIA.site_id = JS_CONSTANTS.TWITTER;
            window.open('/login/authenticate_twitter');
            JS_VARS.retweeter = 1;
            MEDIA.checklink = setTimeout(checkLink, 2000);
        }
    },
    
    'postRetweet': function() {
        $.getJSON('/api/stream/retweet.json?tweet_id='+RETWEET.tweet_id, function(){
            RETWEET.tweet_post.find('.retweet').text('retweeted!').addClass('done');
        });
    }
}


var STREAM = {
    'initted':0,
    'swap': function(e) {
        e.preventDefault();
        clearInterval(STREAM.autoupdateloop);
        COMMENT.cancel();
        var link = $(this);    
        STREAM.url = link.attr('href');
        STREAM.index = link.index();
        if (link.hasClass('disabled') || STREAM.lock==1) {
            return;
        } else {
            $('#streamnav li').removeClass('on');
            link.parent('li').addClass('on');
            STREAM.lock = 1;
            boxheight = $('#tweetbox ul').height();
            $('.tweetfeed').fadeOut('fast', function(){
                $('#twitter_stream_box').removeAttr('class').attr('class', 'togbox stmid_'+link.getValueFromClass('id'));
                var old = $(this);
                $('#streamload').show();
                try{updateCufon();}catch(e){}
                $.get(STREAM.url, function(output, status, jqx){
                    $('#streamload').hide();
                    STREAM.holdingTank = [];
                    STREAM.iteration = 0;
                    STREAM.lastfetch = jqx.getResponseHeader('Since');
                    var tbox = (tweetbox.find('.jspPane').length>0) ? tweetbox.find('.jspPane') : tweetbox;
                    var newbatch = $('<ul/>')
                        .hide()
                        .html(output)
                        .appendTo(tbox)
                        .addClass('tweetfeed')
                        .fadeIn('fast', function(){
                            old.remove();
                            STREAM.lock = 0;
                            STREAM.autolock = 0;
                            boxheight = newbatch.height();
                            if (!isTablet) {
                                var jscOptions = jscOptions || {};
                                jscOptions['verticalDragMinHeight'] = 92;
                                jscOptions['verticalDragMaxHeight'] = 92;
                                tweetbox.jScrollPane(jscOptions);
                            }
                        });
                        
                        $('.s-item.source_41 p a').each(function(){
                            var aLink = $(this).text();
                            $(this).attr({
                                'target': '_blank',
                                'class': 'outlink',
                                'rel': 'Blog_Stream_Link_'+aLink
                            });
                        });
                        $('.s-item.source_41 p').find('a').attr({
                            'target': '_blank',
                            'class': 'outlink',
                            'rel': aLink
                        })
                     if (link.hasClass('auto')) {                        
                        var ppm = jqx.getResponseHeader('PPM');
                        STREAM.updaterate = 5000;                        
                        STREAM.autoupdateloop = setInterval(STREAM.autoUpdate, STREAM.updaterate);
                     }
                });
            });
        }
    },
    
    'holdingTank': [],
    
    'autoUpdate': function() {
        if (STREAM.lock==1 || STREAM.autolock==1 || STREAM.iteration > 500) {return;}
        STREAM.autolock = 1;
        STREAM.iteration++;
        var old = $('.tweetfeed');
        $.getJSON(STREAM.url.replace('tpl','json')+'&since='+STREAM.lastfetch, function(output, status, jqx){
            if (STREAM.lock==1) {return;}
            if (output=='{"status":"OK","response":null}') {return;}           
            if (output.results && output.results.length) {
                STREAM.lastfetch = jqx.getResponseHeader('Since');
                $.each(output.results, function(){
                    STREAM.holdingTank.push(this);
                });
                STREAM.inventory = STREAM.holdingTank.length;
                clearInterval(STREAM.trickleloop);                
                STREAM.tricklerate = (STREAM.inventory > (STREAM.updaterate/1000)) ? 2000 : (Math.floor(((STREAM.updaterate*2)/1000) / STREAM.inventory+1)) * 1000;
                STREAM.trickleloop = setInterval(STREAM.trickle, STREAM.tricklerate);
            } else {
                STREAM.inventory = STREAM.holdingTank.length;
            }
            STREAM.lock = 0;
            STREAM.autolock = 0;
            var ppm = jqx.getResponseHeader('PPM');
            var newrate = (ppm < 2) ? 15000 : (Math.floor(60/(ppm+1)) + 4) * 1000;
            newrate = (newrate<10000) ? 10000 : newrate;
            if (newrate > (STREAM.updaterate*1.2) || newrate < (STREAM.updaterate/1.2)) {
                clearInterval(STREAM.autoupdateloop);
                STREAM.updaterate = newrate;
                STREAM.autoupdateloop = setInterval(STREAM.autoUpdate, STREAM.updaterate);
            }
        });        
    },
    
    'trickle': function(){
        if (STREAM.holdingTank.length) {
            var item = STREAM.holdingTank.pop();
            var rndr = Math.random()*(STREAM.tricklerate/1.5);

            // STREAM V2, BUT ALSO WORKS IN V1
            if ($.inArray(item.stream_post_id, UPDATES.successful_posts) == -1) {
                tweetbox.animate({'opacity':1}, rndr, POST.build(item, true));
                var posts = $('.tweetfeed:first li.s-item');
                if (posts.length > 49) {
                    posts.last().remove();
                }
                if (!isTablet && STREAM.iteration%3) {
                    var jscOptions = jscOptions || {};
                    jscOptions['verticalDragMinHeight'] = 92;
                    jscOptions['verticalDragMaxHeight'] = 92;
                    tweetbox.jScrollPane(jscOptions);
                }
                if (STREAM.inventory > 50) {
                    STREAM.holdingTank = STREAM.holdingTank.slice(0, 50);
                    STREAM.inventory = STREAM.holdingTank.length;
                }
            }
        } else {
            clearInterval(STREAM.trickleloop);
        }
    }   
}

/* popup for yt videos */
function ytPop(e) {
    e.preventDefault();
    $('#playbox').remove();
    var media_id = $(this).find('img').attr('src').split('vi/')[1].split('/default')[0].split('/2.jpg')[0].split('/0.jpg')[0],
        playbox = $('<div id="playbox"/>')
        .html($.buildVideoEmbed(1,media_id))
        .prependTo($('body'))
        .css({'top':920,'left':e.pageX-70, 'position':'absolute'});
    $('<a href="#" class="close">X</a>')
        .appendTo(playbox)
        .click(function(e){
            e.preventDefault();
            $('#playbox').fadeOut('fast',function(){
                $(this).remove();
            });
    });
}

/* popup for flickr photos */
function flickrPop(e){
    e.preventDefault();
    $('#playbox').remove();
    var flickrbig = $(this).find('img').attr('src').split('_t')[0],
        playbox = $('<div id="playbox"/>')
            .css({'top':920,'left':e.pageX-70, 'position':'absolute', 'border-radius':'4px'});
    $('<img src="'+flickrbig+'_b.jpg">')
        .css({'max-height':800, 'max-width':$('#container').width()*.85})
        .appendTo(playbox);
    $('<div><a target="_blank" href="'+$(this).attr('href')+'">View on Flickr &raquo;</a></div>')
        .css({'text-align':'center','padding':'4px'})
        .appendTo(playbox);
    playbox.prependTo($('body'));        
    $('<a href="#" class="close">X</a>')
        .appendTo(playbox)
        .click(function(e){
            e.preventDefault();
            $('#playbox').fadeOut('fast',function(){
                $(this).remove();
            });
    });
}

// checks if you are linked
function checkLink() {
    console.log("checkLink called")
    $.getJSON('/api/base/get_linked_status.json?site_id='+MEDIA.site_id+'&get_fields[]=linked&get_fields[]=user', function (output) {
        if (output.linked) {
            POST.userInfo = output;
            JS_VARS.linked = MEDIA.site_id;
            $('#cform, #streampost, .streampost').addClass('linked');
            $('#checking').remove();

            // STREAM V1
            if (!$('#streampost').hasClass('stream_auth_v2')) {
                if (JS_VARS.retweeter==1) {
                    RETWEET.postRetweet();
                    JS_VARS.retweeter = 0;
                } else if ($('#streampost textarea').length && $('#streampost textarea').val()!='' && $('#streampost textarea').val()!=' ' && $('#streampost textarea').hasClass('cleared')) {
                    POST.send();
                } else if ($('#cform textarea:visible').length) {
                    $('#cform textarea').focus();
                }
            }
            // STREAM V2
            else {
                console.log("And POST.link_process is... ", POST.link_process);
                var spost = $('#streampost textarea');
                
                if (JS_VARS.retweeter==1) {
                    RETWEET.postRetweet();
                    JS_VARS.retweeter = 0;
                }
                
                else if (POST.link_process == 'msg_post') {
                    console.log("about to call POST.send() from line 896");
                    POST.send();
                }
                else if (POST.link_process == 'empty_post' || POST.link_process == 'empty_signin') {
                    spost.removeClass('cleared');
                    POST.fillUserInfo(output, UPDATES.streampost_cta);
                }
                else if (POST.link_process == 'msg_signin') {
                    POST.fillUserInfo(output, spost.val());
                }
            }
            
            clearChecklink();
            try{user = output.user;}catch(e){}
        } else {
            clearTimeout(MEDIA.checklink);
            MEDIA.checklink = setTimeout(checkLink, 2000);
        }
    });
}

// cancels the loop checking for connected account, cleans up
function clearChecklink() {
    clearInterval(MEDIA.checklink);
    MEDIA.checklink = '';
    $('#checking').remove();
}

/*
var TRACK = {
    'buildTrackLink': function(dest,rtid,ent,rtype){
        return '/api/base/trackr.json?ref_type_id='+rtype+'_'+rtid+'&entity_id='+ent+'&url='+escape(dest)+'&environment_id='+ENVIRONMENT_ID+'&region_id='+REGION_ID+'&ref_type='+rtype+'&page_id='+JS_VARS.PAGE_ID;
    },

    'outlink': function() {
        var link = $(this),
            dest = link.attr('href'),
            ent = (link.getValueFromClass('entity')!='') ? link.getValueFromClass('entity') : 0,
            rtype = (link.getValueFromClass('rtype')!='') ? link.getValueFromClass('rtype') : 5,
            rtid = (link.attr('rel')!='') ? link.attr('rel').replace(' ','-') : link.text().replace(/ /g,'-');
        if (link.parents('li.rc').length) {
            rtid = 'RSP_' + rtid;
        } else if (link.parents('div.widebands').length) {
            rtid = 'BBP_' + rtid;
        }   
        $(this).attr('href', TRACK.buildTrackLink(dest,rtid,ent,rtype));
    },

    'sig_clicks_async': function(ref_type, ref_type_id, entity_id) {
        var ent = entity_id ? entity_id : 0,
            rtype = ref_type ? ref_type : 0,
            rtid = ref_type_id ? rtype + '_' + ref_type_id.replace(' ','-') : 'click';
        $.post('/api/base/tracklink.json', {'ref_type': rtype, 'ref_type_id': rtid, 'entity_id': ent, 'environment_id': ENVIRONMENT_ID, 'region_id': REGION_ID, 'page_id': JS_VARS.PAGE_ID});
    },

    'sig_clicks': function (tracking_id) {
        $.cookie('sig_clicks', tracking_id, { expires: 0.00864, path: '/' });
    }

}

function initStream() {
    if (STREAM.initted==1) {return; }
    STREAM.initted = 1;
    $('#streamnav a').eq(0).click();
    $.post('/redir.html');
}
*/

$(document).ready(function() {
    
    try{
        Cufon.replace(Cufon.selection);
        Cufon.now();
    }catch(e){}
        
    //outgoing click tracking
    $('a.outlink').each(TRACK.outlink);
    $('.social a').each(TRACK.outlink);
    $('.topshare a:not(.fb)').each(TRACK.outlink);
    
    tweetbox = $('#tweetbox');
    
    
    // this initialises the scollpane on the page.
    if (!isTablet) {
        var jscOptions = jscOptions || {};
        jscOptions['verticalDragMinHeight'] = 92;
        jscOptions['verticalDragMaxHeight'] = 92;
        tweetbox.jScrollPane(jscOptions);
    }

    /* stream tabs */
    $('#streamnav a').live('click',STREAM.swap);

    /* load first stream */
    if ($('#noflash').length && STREAM.initted!=1) {
        STREAM.initted = 1;
        $('#streamnav a').eq(0).click();  //this is done by the flash. if no flash, do it manually
    }

    /* youtube video popup */
    $('#tweetbox .source_1 .imgs a').live('click', ytPop);
    /* flickr photo popup */
    $('#tweetbox .source_2 .imgs a').live('click', flickrPop);
    

    /* attach actions for comments */
    $('#tweetbox').delegate('.addcomment','click', COMMENT.showform);
    $('#tweetbox').delegate('.retweet','click', RETWEET.checkAuth);
    $('#lower').delegate('#comment_save','click',COMMENT.save);
    $('#lower').delegate('#comment_cancel','click',COMMENT.cancel);
    $('#tweetbox').delegate('.delete','click',COMMENT.remove);
    $('#tweetbox').delegate('.deletestreampost','click', POST.remove);
    $('#tweetbox').delegate('.banstreampost','click', POST.ban);    
    $('#tweetbox').delegate('.morecomments a','click', COMMENT.more);
    $('#cform').delegate('.out','click',COMMENT.outlink);
    $('#cform').delegate('.site_3','click',COMMENT.fblink);

    /* attach actions for stream posting */    
    $('#stream_save').click(POST.save);
    $('#streampost .out').click(POST.outlink);
    $('#streampost .site_3').click(POST.fblink);
    $('#streampost textarea').focus(clearField).keydown(function(e){
        var keyCode = e.keyCode || e.which;
        if (keyCode==13) { POST.save(); }
    });
    // STREAM AUTH V2
    $('#streampost .change_user_btn').click(POST.change_user);
    $('#streampost .user_logout_btn').click(POST.streampost_logout);
    
    $('#regionswap').click(function(e){
        e.preventDefault();
        $('#regionswapper').slideToggle();
    });    

});

